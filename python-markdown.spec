%{!?python3_version: %global python3_version %(%{__python3} -c "import sys; sys.stdout.write(sys.version[:3])")}

Name:           python-markdown
Version:        3.7
Release:        1
Summary:        A Python implementation of John Gruber’s Markdown
License:        BSD-3-Clause
URL:            https://pypi.org/project/Markdown/
Source0:        %{pypi_source markdown}

BuildArch:      noarch

BuildRequires:  python3-devel >= 3.1 python3-nose2 python3-pyyaml python3-pip python3-wheel

%description
This is a Python implementation of John Gruber’s Markdown.
It is almost completely compliant with the reference implementation,
though there are a few known issues.

%package -n python3-markdown
Summary:        Markdown implementation in Python
Requires:       python3 >= 3.1
%{?python_provide:%python_provide python3-markdown}

%description -n python3-markdown
This is a Python implementation of John Gruber’s Markdown.
It is almost completely compliant with the reference implementation,
though there are a few known issues.

%prep
%autosetup -n markdown-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install

PYTHONPATH=%{buildroot}%{python3_sitelib} \
  %{buildroot}%{_bindir}/markdown_py \
  LICENSE.md > LICENSE.html

%check
%{__python3} -m unittest discover tests

%files -n python3-markdown
%license LICENSE.html LICENSE.md
%{python3_sitelib}/*
%{_bindir}/markdown_py

%changelog
* Thu Oct 10 2024 Ge Wang <wang__Ge@126.com> - 3.7-1
- Upgrade to 3.7

* Mon Jan 8 2024 liyanan <liyanan61@h-partners.com> - 3.5.1-1
- Upgrade to 3.5.1

* Wed May 31 2023 chenchen <chen_aka_jan@163.com> - 3.4.1-1
- Upgrade to 3.4.1

* Tue Jan 17 2023 caofei <caofei@xfusion.com> -   3.3.7-2
- Update th/td to use style attribute

* Thu Jun 09 2022 SimpleUpdate Robot <tc@openeuler.org> - 3.3.7-1
- Upgrade to version 3.3.7

* Tue May 10 2022 Ge Wang <wangge20@h-partner.com> - 3.3.1-5
- License compliance rectification

* Wed Jan 05 2022 Ge Wang <wangge20@huawei.com> - 3.3.1-4
- Change BuildRequires from python3-nose to python3-nose2 due to python3-nose is abandoned

* Mon Sep 27 2021 lingsheng <lingsheng@huawei.com> - 3.3.1-3
- Provide python-markdown

* Tue Oct 27 2020 wangxiao <wangxiao65@huawei.com> - 3.3.1-2
- remove python2 buildrequires

* Thu Oct 15 2020 Zhipeng Xie <xiezhipeng1@huawei.com> - 3.3.1-1
- upgrade to 3.3.1

* Tue Sep 29 2020 liuweibo <liuweibo10@huawei.com> - 2.4.1-14
- Fix Source0

* Tue Nov 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.4.1-13
- Package init
